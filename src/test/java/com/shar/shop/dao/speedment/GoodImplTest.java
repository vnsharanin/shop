package com.shar.shop.dao.speedment;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.perf4j.StopWatch;
import org.perf4j.log4j.Log4JStopWatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.junit.Ignore;
import com.shar.shop.entities.speedment.shop.ShopApplication;
import com.shar.shop.entities.speedment.shop.shop.shop.goods.Goods;
import com.shar.shop.entities.speedment.shop.shop.shop.goods.impl.GoodsImpl;
import com.speedment.Speedment;

public class GoodImplTest {

    private static final Logger logger = Logger.getLogger(UserImplTest.class);
    Speedment speedment = new ShopApplication().build();
    Goods goodDAO = new GoodsImpl(speedment);

	@Ignore
    @Test//(timeout=100)
    public void getGoods100Threads() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("SPEEDMENT. GET GOODS. Threads: 100");
        for (int i = 0; i < 100; i++) {
            service.submit(goodDAO::getGoods);
        }
        stopWatch.stop();
        service.shutdown();
    }
	@Ignore
    @Test//(timeout=100)
    public void getGoods1000Threads() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("SPEEDMENT. GET GOODS. Threads: 1 000");
        for (int i = 0; i < 1000; i++) {
            service.submit(goodDAO::getGoods);
        }
        stopWatch.stop();
        service.shutdown();
    }
	@Ignore
    @Test//(timeout=100)
    public void getGoods10000Threads() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("SPEEDMENT. GET GOODS. Threads: 10 000");
        for (int i = 0; i < 10000; i++) {
            service.submit(goodDAO::getGoods);
        }
        stopWatch.stop();
        service.shutdown();
    }
    

    @Ignore
    @Test//(timeout=100)
    public void saveGoodTest100Threads1000Records() {//Summary records: 100 000
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("SPEEDMENT. INSERT TO THE GOODS. Threads: 100. Records: 1 000");
        for (int i = 0; i < 100; i++) {
            service.submit(() -> {
                for (int j = 0; j < 1000; j++) {
                    goodDAO.saveGood("testGood", 1000, "testSerial");
                }
            });
        }
        stopWatch.stop();
        service.shutdown();
    }

    @Ignore
    @Test//(timeout=100)
    public void saveGoodTest1000Threads1000Records() {//Summary records: 1 000 000
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("SPEEDMENT. INSERT TO THE GOODS. Threads: 1 000. Records: 1 000");
        for (int i = 0; i < 1000; i++) {
            service.submit(() -> {
                for (int j = 0; j < 1000; j++) {
                    goodDAO.saveGood("testGood", 1000, "testSerial");
                }
            });
        }
        stopWatch.stop();
        service.shutdown();
    }

    @Ignore
    @Test//(timeout=100)
    public void saveGoodTest10000Threads1000Records() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("SPEEDMENT. INSERT TO THE GOODS. Threads: 10 000. Records: 1 000");
        for (int i = 0; i < 10000; i++) {
            service.submit(() -> {
                for (int j = 0; j < 1000; j++) {
                    goodDAO.saveGood("testGood", 1000, "testSerial");
                }
            });
        }
        stopWatch.stop();
        service.shutdown();
    }

    @Ignore
    @Test//(timeout=100)
    public void saveGoodTest100Threads100000Records() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("SPEEDMENT. INSERT TO THE GOODS. Threads: 100. Records: 100 000");
        for (int i = 0; i < 100; i++) {
            service.submit(() -> {
                for (int j = 0; j < 100000; j++) {
                    goodDAO.saveGood("testGood", 1000, "testSerial");
                }
            });
        }
        stopWatch.stop();
        service.shutdown();
    }

    @Ignore
    @Test//(timeout=100)
    public void saveGoodTest1000Threads100000Records() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("SPEEDMENT. INSERT TO THE GOODS. Threads: 1 000. Records: 100 000");
        for (int i = 0; i < 1000; i++) {
            service.submit(() -> {
                for (int j = 0; j < 100000; j++) {
                    goodDAO.saveGood("testGood", 1000, "testSerial");
                }
            });
        }
        stopWatch.stop();
        service.shutdown();
    }

    @Ignore
    @Test//(timeout=100)
    public void saveGoodTest10000Threads100000Records() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("SPEEDMENT. INSERT TO THE GOODS. Threads: 10 000. Records: 100 000");
        for (int i = 0; i < 10000; i++) {
            service.submit(() -> {
                for (int j = 0; j < 100000; j++) {
                    goodDAO.saveGood("testGood", 1000, "testSerial");
                }
            });
        }
        stopWatch.stop();
        service.shutdown();
    }    
}
