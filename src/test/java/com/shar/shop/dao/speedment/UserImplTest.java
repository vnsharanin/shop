package com.shar.shop.dao.speedment;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.perf4j.StopWatch;
import org.perf4j.log4j.Log4JStopWatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.junit.Ignore;
import com.shar.shop.entities.speedment.shop.ShopApplication;
import com.shar.shop.entities.speedment.shop.shop.shop.users.Users;
import com.shar.shop.entities.speedment.shop.shop.shop.users.impl.UsersImpl;
import com.speedment.Speedment;

public class UserImplTest {

    private static final Logger logger = Logger.getLogger(UserImplTest.class);
    Speedment speedment = new ShopApplication().build();
    Users userDAO = new UsersImpl(speedment);

	@Ignore
    @Test//(timeout=100)
    public void getUsers100Threads() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("SPEEDMENT. GET USERS. Threads: 100");
        for (int i = 0; i < 100; i++) {
            service.submit(userDAO::getUsers);
        }
        stopWatch.stop();
        service.shutdown();
    }
	@Ignore
    @Test//(timeout=100)
    public void getUsers1000Threads() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("SPEEDMENT. GET USERS. Threads: 1 000");
        for (int i = 0; i < 1000; i++) {
            service.submit(userDAO::getUsers);
        }
        stopWatch.stop();
        service.shutdown();
    }
	@Ignore
    @Test//(timeout=100)
    public void getUsers10000Threads() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("SPEEDMENT. GET USERS. Threads: 10 000");
        for (int i = 0; i < 10000; i++) {
            service.submit(userDAO::getUsers);
        }
        stopWatch.stop();
        service.shutdown();
    }
    

    @Ignore
    @Test//(timeout=100)
    public void saveUserTest100Threads1000Records() {//Summary records: 100 000
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("SPEEDMENT. INSERT TO THE USERS. Threads: 100. Records: 1 000");
        for (int i = 0; i < 100; i++) {
            service.submit(() -> {
                for (int j = 0; j < 1000; j++) {
                    userDAO.saveUser(100,1000,"testStatus");
                }
            });
        }
        stopWatch.stop();
        service.shutdown();
    }

    @Ignore
    @Test//(timeout=100)
    public void saveUserTest1000Threads1000Records() {//Summary records: 1 000 000
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("SPEEDMENT. INSERT TO THE USERS. Threads: 1 000. Records: 1 000");
        for (int i = 0; i < 1000; i++) {
            service.submit(() -> {
                for (int j = 0; j < 1000; j++) {
                    userDAO.saveUser(100,1000,"testStatus");
                }
            });
        }
        stopWatch.stop();
        service.shutdown();
    }

    @Ignore
    @Test//(timeout=100)
    public void saveUserTest10000Threads1000Records() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("SPEEDMENT. INSERT TO THE USERS. Threads: 10 000. Records: 1 000");
        for (int i = 0; i < 10000; i++) {
            service.submit(() -> {
                for (int j = 0; j < 1000; j++) {
                    userDAO.saveUser(100,1000,"testStatus");
                }
            });
        }
        stopWatch.stop();
        service.shutdown();
    }

    @Ignore
    @Test//(timeout=100)
    public void saveUserTest100Threads100000Records() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("SPEEDMENT. INSERT TO THE USERS. Threads: 100. Records: 100 000");
        for (int i = 0; i < 100; i++) {
            service.submit(() -> {
                for (int j = 0; j < 100000; j++) {
                    userDAO.saveUser(100,1000,"testStatus");
                }
            });
        }
        stopWatch.stop();
        service.shutdown();
    }

    @Ignore
    @Test//(timeout=100)
    public void saveUserTest1000Threads100000Records() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("SPEEDMENT. INSERT TO THE USERS. Threads: 1 000. Records: 100 000");
        for (int i = 0; i < 1000; i++) {
            service.submit(() -> {
                for (int j = 0; j < 100000; j++) {
                    userDAO.saveUser(100,1000,"testStatus");
                }
            });
        }
        stopWatch.stop();
        service.shutdown();
    }

    @Ignore
    @Test//(timeout=100)
    public void saveUserTest10000Threads100000Records() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("SPEEDMENT. INSERT TO THE USERS. Threads: 10 000. Records: 100 000");
        for (int i = 0; i < 10000; i++) {
            service.submit(() -> {
                for (int j = 0; j < 100000; j++) {
                    userDAO.saveUser(100,1000,"testStatus");
                }
            });
        }
        stopWatch.stop();
        service.shutdown();
    }    
}
