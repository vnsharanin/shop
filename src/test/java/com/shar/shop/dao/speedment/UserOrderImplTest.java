package com.shar.shop.dao.speedment;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.perf4j.StopWatch;
import org.perf4j.log4j.Log4JStopWatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.junit.Ignore;
import com.shar.shop.entities.speedment.shop.ShopApplication;
import com.shar.shop.entities.speedment.shop.shop.shop.user_order.UserOrder;
import com.shar.shop.entities.speedment.shop.shop.shop.user_order.impl.UserOrderImpl;
import com.speedment.Speedment;

public class UserOrderImplTest {

    private static final Logger logger = Logger.getLogger(UserOrderImplTest.class);
    Speedment speedment = new ShopApplication().build();
    UserOrder orderDAO = new UserOrderImpl(speedment);

	@Ignore
    @Test//(timeout=100)
    public void getOrders100Threads() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("SPEEDMENT. GET ORDERS. Threads: 100");
        for (int i = 0; i < 100; i++) {
            service.submit(orderDAO::getOrders);
        }
        stopWatch.stop();
        service.shutdown();
    }
	@Ignore
    @Test//(timeout=100)
    public void getOrders1000Threads() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("SPEEDMENT. GET ORDERS. Threads: 1 000");
        for (int i = 0; i < 1000; i++) {
            service.submit(orderDAO::getOrders);
        }
        stopWatch.stop();
        service.shutdown();
    }
	@Ignore
    @Test//(timeout=100)
    public void getOrders10000Threads() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("SPEEDMENT. GET ORDERS. Threads: 10 000");
        for (int i = 0; i < 10000; i++) {
            service.submit(orderDAO::getOrders);
        }
        stopWatch.stop();
        service.shutdown();
    }
    
    @Ignore
    @Test//(timeout=100)
    public void saveOrderTest() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("SPEEDMENT. INSERT TO THE USER_ORDER. Records: 100");
        for (int i = 0; i < 100; i++) {
            orderDAO.saveOrder(i,i, "20160404", "createdTestOrder");
        }
        stopWatch.stop();
        service.shutdown();
    }  
}
