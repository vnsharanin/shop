package com.shar.shop.dao.mybatis;

import com.shar.shop.api.GoodDAO;
import com.shar.shop.api.UserDAO;
import com.shar.shop.api.UserOrderDAO;
import com.shar.shop.entities.hibernate.UserOrder;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.perf4j.StopWatch;
import org.perf4j.log4j.Log4JStopWatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.junit.Ignore;

public class UserOrderDAOImplMyBatisTest {

    private static final Logger logger = Logger.getLogger(UserOrderDAOImplMyBatisTest.class);
    final UserOrderDAO orderDAO = new UserOrderDAOImplMyBatis();
    final UserDAO iuser = new UserDAOImplMyBatis();
    final GoodDAO igood = new GoodDAOImplMyBatis();

    @Ignore
    @Test//(timeout=100)
    public void getAllOrders100Threads() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("MYBATIS. GET ORDERS. Threads: 100");
        for (int i = 0; i < 100; i++) {
            service.submit(orderDAO::getAllOrders);
        }
        stopWatch.stop();
        service.shutdown();
    }
	@Ignore
    @Test//(timeout=100)
    public void getAllOrders1000Threads() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("MYBATIS. GET ORDERS. Threads: 1 000");
        for (int i = 0; i < 1000; i++) {
            service.submit(orderDAO::getAllOrders);
        }
        stopWatch.stop();
        service.shutdown();
    }
	@Ignore
    @Test//(timeout=100)
    public void getAllOrders10000Threads() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("MYBATIS. GET ORDERS. Threads: 10 000");
        for (int i = 0; i < 10000; i++) {
            service.submit(orderDAO::getAllOrders);
        }
        stopWatch.stop();
        service.shutdown();
    }

    @Ignore
    @Test//(timeout=100)
    public void saveOrderTest() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("MYBATIS. INSERT TO THE USER_ORDER. Records: 100");
        for (int i = 0; i < 100; i++) {
            orderDAO.saveOrder(new UserOrder("20160404", "createdTestOrder", iuser.getUserById(i), igood.getGoodById(i)));
        }
        stopWatch.stop();
        service.shutdown();
    }
}
