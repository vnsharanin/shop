package com.shar.shop.dao.hibernate;

import com.shar.shop.entities.hibernate.User;
import com.shar.shop.api.UserDAO;
import org.apache.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;
import org.perf4j.StopWatch;
import org.perf4j.log4j.Log4JStopWatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

//mvn clean compile test
public class UserDAOImplHibernateTest {

    private static final Logger logger = Logger.getLogger(UserDAOImplHibernateTest.class);
    final UserDAO userDAO = new UserDAOImplHibernate();

    @Ignore
    @Test//(timeout=100)
    public void getUsers100Threads() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("HIBERNATE. GET USERS. Threads: 100");
        for (int i = 0; i < 100; i++) {
            service.submit(userDAO::getUsers);
        }
        stopWatch.stop();
        service.shutdown();
    }

    @Ignore
    @Test//(timeout=100)
    public void getUsers1000Threads() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("HIBERNATE. GET USERS. Threads: 1 000");
        for (int i = 0; i < 1000; i++) {
            service.submit(userDAO::getUsers);
        }
        stopWatch.stop();
        service.shutdown();
    }

    @Ignore
    @Test//(timeout=100)
    public void getUsers10000Threads() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("HIBERNATE. GET USERS. Threads: 10 000");
        for (int i = 0; i < 10000; i++) {
            service.submit(userDAO::getUsers);
        }
        stopWatch.stop();
        service.shutdown();
    }

    @Ignore
    @Test//(timeout=100)
    public void saveUserTest20Threads20Records() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("HIBERNATE. INSERT TO THE USERS. Threads: 20. Records: 20");
        for (int i = 0; i < 20; i++) {
            service.submit(() -> {
                for (int j = 0; j < 20; j++) {
                    userDAO.saveUser(new User(100, 1000, "testStatus"));
                }
            });
        }
        stopWatch.stop();
        service.shutdown();
    }

    @Ignore
    @Test//(timeout=100)
    public void saveUserTest100Threads1000Records() {//Summary records: 100 000
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("HIBERNATE. INSERT TO THE USERS. Threads: 100. Records: 1 000");
        for (int i = 0; i < 100; i++) {
            service.submit(() -> {
                for (int j = 0; j < 1000; j++) {
                    userDAO.saveUser(new User(100, 1000, "testStatus"));
                }
            });
        }
        stopWatch.stop();
        service.shutdown();
    }

    @Ignore
    @Test//(timeout=100)
    public void saveUserTest1000Threads1000Records() {//Summary records: 1 000 000
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("HIBERNATE. INSERT TO THE USERS. Threads: 1 000. Records: 1 000");
        for (int i = 0; i < 1000; i++) {
            service.submit(() -> {
                for (int j = 0; j < 1000; j++) {
                    userDAO.saveUser(new User(100, 1000, "testStatus"));
                }
            });
        }
        stopWatch.stop();
        service.shutdown();
    }

    @Ignore
    @Test//(timeout=100)
    public void saveUserTest10000Threads1000Records() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("HIBERNATE. INSERT TO THE USERS. Threads: 10 000. Records: 1 000");
        for (int i = 0; i < 10000; i++) {
            service.submit(() -> {
                for (int j = 0; j < 1000; j++) {
                    userDAO.saveUser(new User(100, 1000, "testStatus"));
                }
            });
        }
        stopWatch.stop();
        service.shutdown();
    }

    @Ignore
    @Test//(timeout=100)
    public void saveUserTest100Threads100000Records() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("HIBERNATE. INSERT TO THE USERS. Threads: 100. Records: 100 000");
        for (int i = 0; i < 100; i++) {
            service.submit(() -> {
                for (int j = 0; j < 100000; j++) {
                    userDAO.saveUser(new User(100, 1000, "testStatus"));
                }
            });
        }
        stopWatch.stop();
        service.shutdown();
    }

    @Ignore
    @Test//(timeout=100)
    public void saveUserTest1000Threads100000Records() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("HIBERNATE. INSERT TO THE USERS. Threads: 1 000. Records: 100 000");
        for (int i = 0; i < 1000; i++) {
            service.submit(() -> {
                for (int j = 0; j < 100000; j++) {
                    userDAO.saveUser(new User(100, 1000, "testStatus"));
                }
            });
        }
        stopWatch.stop();
        service.shutdown();
    }

    @Ignore
    @Test//(timeout=100)
    public void saveUserTest10000Threads100000Records() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("HIBERNATE. INSERT TO THE USERS. Threads: 10 000. Records: 100 000");
        for (int i = 0; i < 10000; i++) {
            service.submit(() -> {
                for (int j = 0; j < 100000; j++) {
                    userDAO.saveUser(new User(100, 1000, "testStatus"));
                }
            });
        }
        stopWatch.stop();
        service.shutdown();
    }
}
