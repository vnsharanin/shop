package com.shar.shop.test.speedment;

import org.apache.log4j.Logger;
import org.perf4j.StopWatch;
import org.perf4j.log4j.Log4JStopWatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import com.shar.shop.entities.speedment.shop.ShopApplication;
import com.shar.shop.entities.speedment.shop.shop.shop.users.Users;
import com.shar.shop.entities.speedment.shop.shop.shop.users.impl.UsersImpl;
import com.speedment.Speedment;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

public class UserImplTest {

    private static final Logger logger = Logger.getLogger(UserImplTest.class);
    Speedment speedment = new ShopApplication().build();
    Users userDAO = new UsersImpl(speedment);

    public void getUserById() {
        logger.debug(userDAO.findUserById(1));
    }

    public void getUsers10Threads() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("SPEEDMENT. GET USERS. Threads: 10");
        for (int i = 0; i < 10; i++) {
            service.submit(userDAO::getUsers);
        }
        service.shutdown();
        try {
            service.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException ex) {
            java.util.logging.Logger.getLogger(UserImplTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        stopWatch.stop();
    }

    public void getUsers20Threads() {//.newFixedThreadPool(10);
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("SPEEDMENT. GET USERS. Threads: 20");
        for (int i = 0; i < 20; i++) {
            service.submit(userDAO::getUsers);
        }
        service.shutdown();
        try {
            service.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException ex) {
            java.util.logging.Logger.getLogger(UserImplTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        stopWatch.stop();
    }

    public void getUsers30Threads() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("SPEEDMENT. GET USERS. Threads: 30");
        for (int i = 0; i < 30; i++) {
            service.submit(userDAO::getUsers);
        }
        service.shutdown();
        try {
            service.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException ex) {
            java.util.logging.Logger.getLogger(UserImplTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        stopWatch.stop();
    }
    public void saveUserTest10000Records() {
        StopWatch stopWatch = new Log4JStopWatch("SPEEDMENT. INSERT TO THE USERS. Records: 10 000");
        for (int j = 0; j < 10000; j++) {
            userDAO.saveUser(100, 1000, "testStatus");
        }
        stopWatch.stop();
    }

    public void saveUserTest100000Records() {
        StopWatch stopWatch = new Log4JStopWatch("SPEEDMENT. INSERT TO THE USERS. Records: 100 000");
        for (int j = 0; j < 100000; j++) {
            userDAO.saveUser(100, 1000, "testStatus");
        }
        stopWatch.stop();
    }
// FOR COMPARE WITH MyBatis:
    public void saveUserTest100Threads10000Records() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("SPEEDMENT. INSERT TO THE USERS. Threads: 100. Records: 10 000");
        for (int i = 0; i < 100; i++) {
            service.submit(() -> {
                for (int j = 0; j < 100; j++) {
                    userDAO.saveUser(100, 1000, "testStatus");
                }
            });
        }
        service.shutdown();
        try {
            service.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException ex) {
            java.util.logging.Logger.getLogger(UserImplTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        stopWatch.stop();
    }

    public void saveUserTest1000Threads10000Records() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("SPEEDMENT. INSERT TO THE USERS. Threads: 1 000. Records: 10 000");
        for (int i = 0; i < 1000; i++) {
            service.submit(() -> {
                for (int j = 0; j < 10; j++) {
                    userDAO.saveUser(100, 1000, "testStatus");
                }
            });
        }
        service.shutdown();
        try {
            service.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException ex) {
            java.util.logging.Logger.getLogger(UserImplTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        stopWatch.stop();
    }

    public void saveUserTest100Threads100000Records() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("SPEEDMENT. INSERT TO THE USERS. Threads: 100. Records: 100 000");
        for (int i = 0; i < 100; i++) {
            service.submit(() -> {
                for (int j = 0; j < 1000; j++) {
                    userDAO.saveUser(100, 1000, "testStatus");
                }
            });
        }
        service.shutdown();
        try {
            service.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException ex) {
            java.util.logging.Logger.getLogger(UserImplTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        stopWatch.stop();
    }

    public void saveUserTest1000Threads100000Records() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("SPEEDMENT. INSERT TO THE USERS. Threads: 1 000. Records: 100 000");
        for (int i = 0; i < 1000; i++) {
            service.submit(() -> {
                for (int j = 0; j < 100; j++) {
                    userDAO.saveUser(100, 1000, "testStatus");
                }
            });
        }
        service.shutdown();
        try {
            service.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException ex) {
            java.util.logging.Logger.getLogger(UserImplTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        stopWatch.stop();
    }
    public void saveUserTest100Threads1000000Records() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("SPEEDMENT. INSERT TO THE USERS. Threads: 100. Records: 1 000 000");
        for (int i = 0; i < 100; i++) {
            service.submit(() -> {
                for (int j = 0; j < 10000; j++) {
                    userDAO.saveUser(100, 1000, "testStatus");
                }
            });
        }
        service.shutdown();
        try {
            service.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException ex) {
            java.util.logging.Logger.getLogger(UserImplTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        stopWatch.stop();
    }
    public void saveUserTest1000Threads1000000Records() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("SPEEDMENT. INSERT TO THE USERS. Threads: 1 000. Records: 1 000 000");
        for (int i = 0; i < 1000; i++) {
            service.submit(() -> {
                for (int j = 0; j < 1000; j++) {
                    userDAO.saveUser(100, 1000, "testStatus");
                }
            });
        }
        service.shutdown();
        try {
            service.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException ex) {
            java.util.logging.Logger.getLogger(UserImplTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        stopWatch.stop();
    }
}
