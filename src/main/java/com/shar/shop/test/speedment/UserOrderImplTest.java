package com.shar.shop.test.speedment;

import org.apache.log4j.Logger;
import org.perf4j.StopWatch;
import org.perf4j.log4j.Log4JStopWatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import com.shar.shop.entities.speedment.shop.ShopApplication;
import com.shar.shop.entities.speedment.shop.shop.shop.user_order.UserOrder;
import com.shar.shop.entities.speedment.shop.shop.shop.user_order.impl.UserOrderImpl;
import com.speedment.Speedment;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

public class UserOrderImplTest {

    private static final Logger logger = Logger.getLogger(UserOrderImplTest.class);
    Speedment speedment = new ShopApplication().build();
    final UserOrder orderDAO = new UserOrderImpl(speedment);

    public void getOrderById() {
        logger.debug(orderDAO.findOrderById(1));
    }

    public void getOrders10Threads() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("SPEEDMENT. GET ORDERS. Threads: 10");
        for (int i = 0; i < 10; i++) {
            service.submit(orderDAO::getOrders);
        }
        service.shutdown();
        try {
            service.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException ex) {
            java.util.logging.Logger.getLogger(UserOrderImplTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        stopWatch.stop();
    }

    public void getOrders20Threads() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("SPEEDMENT. GET ORDERS. Threads: 20");
        for (int i = 0; i < 20; i++) {
            service.submit(orderDAO::getOrders);
        }
        service.shutdown();
        try {
            service.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException ex) {
            java.util.logging.Logger.getLogger(UserOrderImplTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        stopWatch.stop();
    }

    public void getOrders30Threads() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("SPEEDMENT. GET ORDERS. Threads: 30");
        for (int i = 0; i < 30; i++) {
            service.submit(orderDAO::getOrders);
        }
        service.shutdown();
        try {
            service.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException ex) {
            java.util.logging.Logger.getLogger(UserOrderImplTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        stopWatch.stop();
    }

    public void saveOrderTest10000Records() {
        StopWatch stopWatch = new Log4JStopWatch("SPEEDMENT. INSERT TO THE USER_ORDER. Records: 10 000");
        for (int j = 1; j <= 10000; j++) {
            orderDAO.saveOrder(j, j, "20160404", "createdTestOrder");
        }
        stopWatch.stop();
    }

    public void saveOrderTest100000Records() {
        StopWatch stopWatch = new Log4JStopWatch("SPEEDMENT. INSERT TO THE USER_ORDER. Records: 100 000");
        for (int j = 1; j <= 100000; j++) {
            orderDAO.saveOrder(j, j, "20160404", "createdTestOrder");
        }
        stopWatch.stop();
    }
}
