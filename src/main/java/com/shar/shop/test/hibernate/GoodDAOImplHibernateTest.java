package com.shar.shop.test.hibernate;

import com.shar.shop.api.GoodDAO;
import com.shar.shop.dao.hibernate.GoodDAOImplHibernate;
import com.shar.shop.entities.hibernate.Good;
import org.apache.log4j.Logger;
import org.perf4j.StopWatch;
import org.perf4j.log4j.Log4JStopWatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

public class GoodDAOImplHibernateTest {

    final static Logger logger = Logger.getLogger(GoodDAOImplHibernateTest.class);
    final GoodDAO goodDAO = new GoodDAOImplHibernate();
	
    public void getGoods10Threads() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("HIBERNATE. GET GOODS. Threads: 10");
        for (int i = 0; i < 10; i++) {
            service.submit(goodDAO::getGoods);
        }
        service.shutdown();
        try {
            service.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException ex) {
            java.util.logging.Logger.getLogger(GoodDAOImplHibernateTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        stopWatch.stop();
    }

    public void getGoods20Threads() {//.newFixedThreadPool(10);
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("HIBERNATE. GET GOODS. Threads: 20");
        for (int i = 0; i < 20; i++) {
            service.submit(goodDAO::getGoods);
        }
        service.shutdown();
        try {
            service.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException ex) {
            java.util.logging.Logger.getLogger(GoodDAOImplHibernateTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        stopWatch.stop();
    }

    public void getGoods30Threads() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("HIBERNATE. GET GOODS. Threads: 30");
        for (int i = 0; i < 30; i++) {
            service.submit(goodDAO::getGoods);
        }
        service.shutdown();
        try {
            service.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException ex) {
            java.util.logging.Logger.getLogger(GoodDAOImplHibernateTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        stopWatch.stop();
    }
    public void saveGoodTest10000Records() {
        StopWatch stopWatch = new Log4JStopWatch("HIBERNATE. INSERT TO THE GOODS. Records: 10 000");
        for (int j = 0; j < 10000; j++) {
            goodDAO.saveGood(new Good("testGood", 1000, "testSerial"));
        }
        stopWatch.stop();
    }

    public void saveGoodTest100000Records() {
        StopWatch stopWatch = new Log4JStopWatch("HIBERNATE. INSERT TO THE GOODS. Records: 100 000");
        for (int j = 0; j < 100000; j++) {
            goodDAO.saveGood(new Good("testGood", 1000, "testSerial"));
        }
        stopWatch.stop();
    }
}
