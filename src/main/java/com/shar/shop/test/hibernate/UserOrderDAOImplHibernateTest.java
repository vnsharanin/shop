package com.shar.shop.test.hibernate;

import com.shar.shop.api.GoodDAO;
import com.shar.shop.api.UserDAO;
import com.shar.shop.api.UserOrderDAO;
import com.shar.shop.dao.hibernate.GoodDAOImplHibernate;
import com.shar.shop.dao.hibernate.UserDAOImplHibernate;
import com.shar.shop.dao.hibernate.UserOrderDAOImplHibernate;
import com.shar.shop.entities.hibernate.UserOrder;
import org.apache.log4j.Logger;
import org.perf4j.StopWatch;
import org.perf4j.log4j.Log4JStopWatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

public class UserOrderDAOImplHibernateTest {

    private static final Logger logger = Logger.getLogger(UserOrderDAOImplHibernateTest.class);
    final UserOrderDAO orderDAO = new UserOrderDAOImplHibernate();
    final UserDAO iuser = new UserDAOImplHibernate();
    final GoodDAO igood = new GoodDAOImplHibernate();

    public void getOrderById() {
        logger.debug(orderDAO.getOrderById(1));

    }
    public void getAllOrders5Threads() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("HIBERNATE. GET ORDERS. Threads: 5");
        for (int i = 0; i < 5; i++) {
            service.submit(orderDAO::getAllOrders);
        }
        service.shutdown();
        try {
            service.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException ex) {
            java.util.logging.Logger.getLogger(UserOrderDAOImplHibernateTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        stopWatch.stop();
    }
    
    public void getAllOrders10Threads() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("HIBERNATE. GET ORDERS. Threads: 10");
        for (int i = 0; i < 10; i++) {
            service.submit(orderDAO::getAllOrders);
        }
        service.shutdown();
        try {
            service.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException ex) {
            java.util.logging.Logger.getLogger(UserOrderDAOImplHibernateTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        stopWatch.stop();
    }

    public void getAllOrders20Threads() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("HIBERNATE. GET ORDERS. Threads: 20");
        for (int i = 0; i < 20; i++) {
            service.submit(orderDAO::getAllOrders);
        }
        service.shutdown();
        try {
            service.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException ex) {
            java.util.logging.Logger.getLogger(UserOrderDAOImplHibernateTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        stopWatch.stop();
    }

    public void getAllOrders30Threads() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("HIBERNATE. GET ORDERS. Threads: 30");
        for (int i = 0; i < 30; i++) {
            service.submit(orderDAO::getAllOrders);
        }
        service.shutdown();
        try {
            service.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException ex) {
            java.util.logging.Logger.getLogger(UserOrderDAOImplHibernateTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        stopWatch.stop();
    }

    public void saveOrderTest10000() {
        StopWatch stopWatch = new Log4JStopWatch("HIBERNATE. INSERT TO THE USER_ORDER. Records: 10 000");
        for (int i = 1; i <= 10000; i++) {
            orderDAO.saveOrder(new UserOrder("20160404", "createdTestOrder", iuser.getUserById(i), igood.getGoodById(i)));
        }
        stopWatch.stop();
    }

    public void saveOrderTest100000Records() {
        StopWatch stopWatch = new Log4JStopWatch("HIBERNATE INSERT TO THE USER_ORDER. Records: 100 000");
        for (int i = 1; i <= 100000; i++) {
            orderDAO.saveOrder(new UserOrder("20160404", "createdTestOrder", iuser.getUserById(i), igood.getGoodById(i)));
        }
        stopWatch.stop();
    }

}
