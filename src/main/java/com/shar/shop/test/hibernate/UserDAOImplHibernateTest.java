package com.shar.shop.test.hibernate;

import com.shar.shop.entities.hibernate.User;
import com.shar.shop.api.UserDAO;
import com.shar.shop.dao.hibernate.UserDAOImplHibernate;
import org.apache.log4j.Logger;
import org.perf4j.StopWatch;
import org.perf4j.log4j.Log4JStopWatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

//mvn clean compile test
public class UserDAOImplHibernateTest {

    private static final Logger logger = Logger.getLogger(UserDAOImplHibernateTest.class);
    final UserDAO userDAO = new UserDAOImplHibernate();

    public void getUser() {
        logger.debug(userDAO.getUserById(1));
    }

    public void getUsers10Threads() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("HIBERNATE. GET USERS. Threads: 10");
        for (int i = 0; i < 10; i++) {
            service.submit(userDAO::getUsers);
        }
        service.shutdown();
        try {
            service.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException ex) {
            java.util.logging.Logger.getLogger(UserDAOImplHibernateTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        stopWatch.stop();
    }

    public void getUsers20Threads() {//.newFixedThreadPool(10);
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("HIBERNATE. GET USERS. Threads: 20");
        for (int i = 0; i < 20; i++) {
            service.submit(userDAO::getUsers);
        }
        service.shutdown();
        try {
            service.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException ex) {
            java.util.logging.Logger.getLogger(UserDAOImplHibernateTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        stopWatch.stop();
    }

    public void getUsers30Threads() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("HIBERNATE. GET USERS. Threads: 30");
        for (int i = 0; i < 30; i++) {
            service.submit(userDAO::getUsers);
        }
        service.shutdown();
        try {
            service.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException ex) {
            java.util.logging.Logger.getLogger(UserDAOImplHibernateTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        stopWatch.stop();
    }

    public void saveUserTest10000Records() {
        StopWatch stopWatch = new Log4JStopWatch("HIBERNATE. INSERT TO THE USERS. Records: 10 000");
        for (int j = 0; j < 10000; j++) {
            userDAO.saveUser(new User(100, 1000, "testStatus"));
        }
        stopWatch.stop();
    }

    public void saveUserTest100000Records() {
        StopWatch stopWatch = new Log4JStopWatch("HIBERNATE. INSERT TO THE USERS. Records: 100 000");
        for (int j = 0; j < 100000; j++) {
            userDAO.saveUser(new User(100, 1000, "testStatus"));
        }
        stopWatch.stop();
    }
}
