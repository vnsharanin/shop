package com.shar.shop.test.mybatis;

import com.shar.shop.api.GoodDAO;
import com.shar.shop.api.UserDAO;
import com.shar.shop.api.UserOrderDAO;
import com.shar.shop.dao.mybatis.GoodDAOImplMyBatis;
import com.shar.shop.dao.mybatis.UserDAOImplMyBatis;
import com.shar.shop.dao.mybatis.UserOrderDAOImplMyBatis;
import com.shar.shop.entities.hibernate.UserOrder;
import org.apache.log4j.Logger;
import org.perf4j.StopWatch;
import org.perf4j.log4j.Log4JStopWatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

public class UserOrderDAOImplMyBatisTest {

    private static final Logger logger = Logger.getLogger(UserOrderDAOImplMyBatisTest.class);
    final UserOrderDAO orderDAO = new UserOrderDAOImplMyBatis();
    final UserDAO iuser = new UserDAOImplMyBatis();
    final GoodDAO igood = new GoodDAOImplMyBatis();

    public void getOrderById() {
        logger.debug(orderDAO.getOrderById(1));
    }

    public void getAllOrders10Threads() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("MYBATIS. GET ORDERS. Threads: 10");
        for (int i = 0; i < 10; i++) {
            service.submit(orderDAO::getAllOrders);
        }
        service.shutdown();
        try {
            service.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException ex) {
            java.util.logging.Logger.getLogger(UserOrderDAOImplMyBatisTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        stopWatch.stop();
    }

    public void getAllOrders20Threads() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("MYBATIS. GET ORDERS. Threads: 20");
        for (int i = 0; i < 20; i++) {
            service.submit(orderDAO::getAllOrders);
        }
        service.shutdown();
        try {
            service.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException ex) {
            java.util.logging.Logger.getLogger(UserOrderDAOImplMyBatisTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        stopWatch.stop();
    }

    public void getAllOrders30Threads() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("MYBATIS. GET ORDERS. Threads: 30");
        for (int i = 0; i < 30; i++) {
            service.submit(orderDAO::getAllOrders);
        }
        service.shutdown();
        try {
            service.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException ex) {
            java.util.logging.Logger.getLogger(UserOrderDAOImplMyBatisTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        stopWatch.stop();
    }

    public void saveOrderTest10000() {
        StopWatch stopWatch = new Log4JStopWatch("MYBATIS. INSERT TO THE USER_ORDER. Records: 10 000");
        for (int i = 1; i <= 10000; i++) {
            orderDAO.saveOrder(new UserOrder("20160404", "createdTestOrder", iuser.getUserById(i), igood.getGoodById(i)));
        }
        stopWatch.stop();
    }

    public void saveOrderTest100000Records() {
        StopWatch stopWatch = new Log4JStopWatch("MYBATIS INSERT TO THE USER_ORDER. Records: 100 000");
        for (int i = 1; i <= 100000; i++) {
            orderDAO.saveOrder(new UserOrder("20160404", "createdTestOrder", iuser.getUserById(i), igood.getGoodById(i)));
        }
        stopWatch.stop();
    }
}
