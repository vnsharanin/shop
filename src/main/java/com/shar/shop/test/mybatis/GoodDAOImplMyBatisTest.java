package com.shar.shop.test.mybatis;

import com.shar.shop.api.GoodDAO;
import com.shar.shop.dao.mybatis.GoodDAOImplMyBatis;
import com.shar.shop.entities.hibernate.Good;
import org.apache.log4j.Logger;
import org.perf4j.StopWatch;
import org.perf4j.log4j.Log4JStopWatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

//mvn clean compile test
public class GoodDAOImplMyBatisTest {

    final static Logger logger = Logger.getLogger(GoodDAOImplMyBatisTest.class);
    final GoodDAO goodDAO = new GoodDAOImplMyBatis();

    public void getGoods10Threads() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("MYBATIS. GET GOODS. Threads: 10");
        for (int i = 0; i < 10; i++) {
            service.submit(goodDAO::getGoods);
        }
        service.shutdown();
        try {
            service.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException ex) {
            java.util.logging.Logger.getLogger(GoodDAOImplMyBatisTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        stopWatch.stop();
    }

    public void getGoods20Threads() {//.newFixedThreadPool(10);
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("MYBATIS. GET GOODS. Threads: 20");
        for (int i = 0; i < 20; i++) {
            service.submit(goodDAO::getGoods);
        }
        service.shutdown();
        try {
            service.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException ex) {
            java.util.logging.Logger.getLogger(GoodDAOImplMyBatisTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        stopWatch.stop();
    }

    public void getGoods30Threads() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("MYBATIS. GET GOODS. Threads: 30");
        for (int i = 0; i < 30; i++) {
            service.submit(goodDAO::getGoods);
        }
        service.shutdown();
        try {
            service.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException ex) {
            java.util.logging.Logger.getLogger(GoodDAOImplMyBatisTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        stopWatch.stop();
    }
    public void saveGoodTest10000Records() {
        StopWatch stopWatch = new Log4JStopWatch("MYBATIS. INSERT TO THE GOODS. Records: 10 000");
        for (int j = 0; j < 10000; j++) {
            goodDAO.saveGood(new Good("testGood", 1000, "testSerial"));
        }
        stopWatch.stop();
    }

    public void saveGoodTest100000Records() {
        StopWatch stopWatch = new Log4JStopWatch("MYBATIS. INSERT TO THE GOODS. Records: 100 000");
        for (int j = 0; j < 100000; j++) {
            goodDAO.saveGood(new Good("testGood", 1000, "testSerial"));
        }
        stopWatch.stop();
    }
// FOR COMPARE WITH SPEEDMENT:
    public void saveGoodTest100Threads10000Records() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("MYBATIS. INSERT TO THE GOODS. Threads: 100. Records: 10 000");
        for (int i = 0; i < 100; i++) {
            service.submit(() -> {
                for (int j = 0; j < 100; j++) {
                    goodDAO.saveGood(new Good("testGood", 1000, "testSerial"));
                }
            });
        }
        service.shutdown();
        try {
            service.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException ex) {
            java.util.logging.Logger.getLogger(GoodDAOImplMyBatisTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        stopWatch.stop();
    }

    public void saveGoodTest1000Threads10000Records() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("MYBATIS. INSERT TO THE GOODS. Threads: 1 000. Records: 10 000");
        for (int i = 0; i < 1000; i++) {
            service.submit(() -> {
                for (int j = 0; j < 10; j++) {
                    goodDAO.saveGood(new Good("testGood", 1000, "testSerial"));
                }
            });
        }
        service.shutdown();
        try {
            service.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException ex) {
            java.util.logging.Logger.getLogger(GoodDAOImplMyBatisTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        stopWatch.stop();
    }

    public void saveGoodTest100Threads100000Records() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("MYBATIS. INSERT TO THE GOODS. Threads: 100. Records: 100 000");
        for (int i = 0; i < 100; i++) {
            service.submit(() -> {
                for (int j = 0; j < 1000; j++) {
                    goodDAO.saveGood(new Good("testGood", 1000, "testSerial"));
                }
            });
        }
        service.shutdown();
        try {
            service.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException ex) {
            java.util.logging.Logger.getLogger(GoodDAOImplMyBatisTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        stopWatch.stop();
    }

    public void saveGoodTest1000Threads100000Records() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("MYBATIS. INSERT TO THE GOODS. Threads: 1 000. Records: 100 000");
        for (int i = 0; i < 1000; i++) {
            service.submit(() -> {
                for (int j = 0; j < 100; j++) {
                    goodDAO.saveGood(new Good("testGood", 1000, "testSerial"));
                }
            });
        }
        service.shutdown();
        try {
            service.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException ex) {
            java.util.logging.Logger.getLogger(GoodDAOImplMyBatisTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        stopWatch.stop();
    }
    public void saveGoodTest100Threads1000000Records() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("MYBATIS. INSERT TO THE GOODS. Threads: 100. Records: 1 000 000");
        for (int i = 0; i < 100; i++) {
            service.submit(() -> {
                for (int j = 0; j < 10000; j++) {
                    goodDAO.saveGood(new Good("testGood", 1000, "testSerial"));
                }
            });
        }
        service.shutdown();
        try {
            service.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException ex) {
            java.util.logging.Logger.getLogger(GoodDAOImplMyBatisTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        stopWatch.stop();
    }    
    public void saveGoodTest1000Threads1000000Records() {
        ExecutorService service = Executors.newCachedThreadPool();
        StopWatch stopWatch = new Log4JStopWatch("MYBATIS. INSERT TO THE GOODS. Threads: 1 000. Records: 1 000 000");
        for (int i = 0; i < 1000; i++) {
            service.submit(() -> {
                for (int j = 0; j < 1000; j++) {
                    goodDAO.saveGood(new Good("testGood", 1000, "testSerial"));
                }
            });
        }
        service.shutdown();
        try {
            service.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException ex) {
            java.util.logging.Logger.getLogger(GoodDAOImplMyBatisTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        stopWatch.stop();
    }    
}
