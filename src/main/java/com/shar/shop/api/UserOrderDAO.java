package com.shar.shop.api;

import com.shar.shop.entities.hibernate.UserOrder;
import java.util.List;

public interface UserOrderDAO {

    void saveOrder(UserOrder order);

    void updateOrder(UserOrder order);
    
    UserOrder getOrderById(int id);

    List<UserOrder> getAllOrders();

    void deleteOrderById(int id);
    
    //List<UserOrder> getOrdersByUserId(int id);
}
