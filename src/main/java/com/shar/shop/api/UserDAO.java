package com.shar.shop.api;

import com.shar.shop.entities.hibernate.User;
import java.util.List;

public interface UserDAO {

    void saveUser(User user);

    void updateUser(User user);
    
    User getUserById(int id);

    List<User> getUsers();

    void deleteUserById(int id);
}
