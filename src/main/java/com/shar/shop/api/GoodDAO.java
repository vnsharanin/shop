package com.shar.shop.api;

import com.shar.shop.entities.hibernate.Good;
import java.util.List;

public interface GoodDAO {

    void saveGood(Good good);

    void updateGood(Good good);

    Good getGoodById(int id);

    List<Good> getGoods();

    void deleteGoodById(int id);
}
