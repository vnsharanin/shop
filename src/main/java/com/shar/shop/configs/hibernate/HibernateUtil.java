package com.shar.shop.configs.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
//import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
//import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
//import org.hibernate.service.ServiceRegistry;

public class HibernateUtil {

    /*private static final SessionFactory sessionFactory = buildSessionFactory();
    private static ServiceRegistry serviceRegistry;

    private static SessionFactory buildSessionFactory() {

        Configuration configuration = new Configuration();
        configuration.configure();
        serviceRegistry = new StandardServiceRegistryBuilder()
                .applySettings(configuration.getProperties()).build();
        return configuration.buildSessionFactory(serviceRegistry);
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public static void closeSession(Session session) {
        if (session != null && session.isOpen()) {
            session.close();
        }
    }
*/
    
     private static final SessionFactory sessionFactory = buildSessionFactory();

     private static SessionFactory buildSessionFactory() {
     try {
     return new Configuration().configure().buildSessionFactory();
     } catch (Throwable ex) {
     throw new ExceptionInInitializerError(ex);
     }
     }

     public static SessionFactory getSessionFactory() {
     return sessionFactory;
     }

     public static SessionFactory getNewSessionFactory() {
     return new Configuration().configure().buildSessionFactory();
     }

     public static void shutdown() {
     getSessionFactory().close();
     }

     public static void closeSession(Session session) {
     if (session != null && session.isOpen()) {
     session.close();
     }
     }
}
/*public class HibernateUtil {

 private static final SessionFactory sessionFactory = buildSessionFactory();
 private static ServiceRegistry serviceRegistry;

 private static SessionFactory buildSessionFactory() {

 Configuration configuration = new Configuration();
 configuration.configure();
 serviceRegistry = new StandardServiceRegistryBuilder()
 .applySettings(configuration.getProperties()).build();
 return configuration.buildSessionFactory(serviceRegistry);
 }

 public static SessionFactory getSessionFactory() {
 return sessionFactory;
 }
    
 public static void shutdown() {
 getSessionFactory().close();
 }

 public static void closeSession(Session session) {
 if (session != null && session.isOpen()) {
 session.close();
 }
 }
 
 }*/
