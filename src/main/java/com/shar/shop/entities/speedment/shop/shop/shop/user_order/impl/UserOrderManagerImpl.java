package com.shar.shop.entities.speedment.shop.shop.shop.user_order.impl;

import com.shar.shop.entities.speedment.shop.shop.shop.user_order.UserOrder;
import com.speedment.Speedment;
import com.speedment.config.Column;
import com.speedment.config.Table;
import com.speedment.config.mapper.TypeMapper;
import com.speedment.exception.SpeedmentException;
import com.speedment.internal.core.config.mapper.identity.IntegerIdentityMapper;
import com.speedment.internal.core.config.mapper.identity.StringIdentityMapper;
import com.speedment.internal.core.manager.sql.AbstractSqlManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.annotation.Generated;

/**
 * A manager implementation representing an entity (for example, a row) in
 * the com.speedment.internal.ui.config.TableProperty@59a6eef.
 * <p>
 * This Class or Interface has been automatically generated by Speedment. Any
 * changes made to this Class or Interface will be overwritten.
 * 
 * @author Speedment
 */
@Generated("Speedment")
public class UserOrderManagerImpl extends AbstractSqlManager<UserOrder> {
    
    private final TypeMapper<Integer, Integer> idTypeMapper = new IntegerIdentityMapper();
    private final TypeMapper<Integer, Integer> userIdTypeMapper = new IntegerIdentityMapper();
    private final TypeMapper<Integer, Integer> goodIdTypeMapper = new IntegerIdentityMapper();
    private final TypeMapper<String, String> dateOrderTypeMapper = new StringIdentityMapper();
    private final TypeMapper<String, String> statusTypeMapper = new StringIdentityMapper();
    
    public UserOrderManagerImpl(Speedment speedment) {
        super(speedment);
        setSqlEntityMapper(this::defaultReadEntity);
    }
    
    @Override
    public Class<UserOrder> getEntityClass() {
        return UserOrder.class;
    }
    
    @Override
    public Object get(UserOrder entity, Column column) {
        switch (column.getName()) {
            case "id" : return entity.getId();
            case "user_id" : return entity.getUser().getId();
            case "good_id" : return entity.getGoodId();
            case "date_order" : return entity.getDateOrder();
            case "status" : return entity.getStatus();
            default : throw new IllegalArgumentException("Unknown column '" + column.getName() + "'.");
        }
    }
    
    @Override
    public void set(UserOrder entity, Column column, Object value) {
        switch (column.getName()) {
            case "id" : entity.setId((Integer) value); break;
            case "user_id" : entity.setUserId((Integer) value); break;
            case "good_id" : entity.setGoodId((Integer) value); break;
            case "date_order" : entity.setDateOrder((String) value); break;
            case "status" : entity.setStatus((String) value); break;
            default : throw new IllegalArgumentException("Unknown column '" + column.getName() + "'.");
        }
    }
    
    @Override
    public Table getTable() {
        return speedment.getProjectComponent().getProject().findTableByName("shop.shop.user_order");
    }
    
    protected UserOrder defaultReadEntity(ResultSet resultSet) {
        final UserOrder entity = newInstance();
        try {
            entity.setId(idTypeMapper.toJavaType(resultSet.getInt(1)));
            entity.setUserId(userIdTypeMapper.toJavaType(resultSet.getInt(2)));
            entity.setGoodId(goodIdTypeMapper.toJavaType(resultSet.getInt(3)));
            entity.setDateOrder(dateOrderTypeMapper.toJavaType(resultSet.getString(4)));
            entity.setStatus(statusTypeMapper.toJavaType(resultSet.getString(5)));
        }
        catch (SQLException sqle) {
            throw new SpeedmentException(sqle);
        }
        return entity;
    }
    
    @Override
    public UserOrder newInstance() {
        return new UserOrderImpl(speedment);
    }
    
    @Override
    public Integer primaryKeyFor(UserOrder entity) {
        return entity.getId();
    }
}