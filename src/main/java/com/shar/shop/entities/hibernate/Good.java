package com.shar.shop.entities.hibernate;

import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashSet;
import java.util.Set;

public class Good implements java.io.Serializable {

    private Integer id;
    private String name;
    private Integer price;
    private String serial;
    private Set<UserOrder> orders = new HashSet<UserOrder>();

    public Good() {
    }

    public Good(String name, Integer price, String serial) {
        this.name = name;
        this.serial = serial;
        this.price = price;
    }

    public Good(Integer id, String name, Integer price, String serial) {
        this.id = id;
        this.name = name;
        this.serial = serial;
        this.price = price;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public Set<UserOrder> getOrders() {
        return orders;
    }

    public void setOrders(Set<UserOrder> orders) {
        this.orders = orders;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Good good = (Good) o;

        return new org.apache.commons.lang.builder.EqualsBuilder()
                .append(id, good.id)
                .append(name, good.name)
                .append(price, good.price)
                .append(serial, good.serial)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new org.apache.commons.lang.builder.HashCodeBuilder(17, 37)
                .append(id)
                .append(name)
                .append(price)
                .append(serial)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append(id)
                .append(name)
                .append(price)
                .append(serial)
                .toString();
    }
}
