package com.shar.shop.entities.hibernate;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class UserOrder implements java.io.Serializable {

    private Integer id;
    private String date;
    private String status;
    private User user;
    private Good good;

    public UserOrder() {
    }

    public UserOrder(String date, String status, User user, Good good) {
        this.date = date;
        this.status = status;
        this.user = user;
        this.good = good;
    }

    public UserOrder(Integer id, String date, String status, User user, Good good) {
        this.id = id;
        this.date = date;
        this.status = status;
        this.user = user;
        this.good = good;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Good getGood() {
        return good;
    }

    public void setGood(Good good) {
        this.good = good;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserOrder userOrder = (UserOrder) o;

        return new EqualsBuilder()
                .append(id, userOrder.id)
                .append(date, userOrder.date)
                .append(status, userOrder.status)
                .append(user, userOrder.user)
                .append(good, userOrder.good)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(date)
                .append(status)
                .append(user)
                .append(good)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append(id)
                .append(date)
                .append(status)
                .append(user)
                .append(good)
                .toString();
    }
}
