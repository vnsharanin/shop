package com.shar.shop.entities.hibernate;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashSet;
import java.util.Set;

public class User implements java.io.Serializable {

    private Integer id;
    private Integer age;
    private Integer balance;
    private String status;
    private Set<UserOrder> orders = new HashSet<UserOrder>();

    public User() {
    }

    public User(Integer age, Integer balance, String status) {
        this.age = age;
        this.balance = balance;
        this.status = status;
    }

    public User(Integer id, Integer age, Integer balance, String status) {
        this.id = id;
        this.age = age;
        this.balance = balance;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Set<UserOrder> getOrders() {
        return orders;
    }

    public void setOrders(Set<UserOrder> orders) {
        this.orders = orders;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        User user = (User) o;

        return new EqualsBuilder()
                .append(id, user.id)
                .append(age, user.age)
                .append(balance, user.balance)
                .append(status, user.status)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(age)
                .append(balance)
                .append(status)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append(id)
                .append(age)
                .append(balance)
                .append(status)
                .toString();
    }
}
