package com.shar.shop.dao.hibernate;

import com.shar.shop.api.GoodDAO;
import com.shar.shop.configs.hibernate.HibernateUtil;
import com.shar.shop.entities.hibernate.Good;
import com.shar.shop.entities.hibernate.UserOrder;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class GoodDAOImplHibernate implements GoodDAO {

    private static final Logger logger = Logger.getLogger(GoodDAOImplHibernate.class);

    private Session session = null;
    private Transaction tx = null;

    @Override
    public Good getGoodById(int id) {
        session = HibernateUtil.getSessionFactory().openSession();
        try {
            Query query = session.createQuery("from Good where id = :id");
            query.setInteger("id", id);
            return (Good) query.uniqueResult();
        } catch (HibernateException he) {
            logger.error(he);
        } finally {
            HibernateUtil.closeSession(session);
        }
        return null;
    }

    @Override
    public void saveGood(Good good) {
        session = HibernateUtil.getSessionFactory().openSession();
        tx = session.beginTransaction();
        try {
            session.save(good);
            tx.commit();
        } catch (HibernateException he) {
            tx.rollback();
            logger.error(he);
        } finally {
            HibernateUtil.closeSession(session);
        }
    }
    
    @Override
    public void updateGood(Good good) {
        session = HibernateUtil.getSessionFactory().openSession();
        tx = session.beginTransaction();
        try {
            session.update(good);
            tx.commit();
        } catch (HibernateException he) {
            tx.rollback();
            logger.error(he);
        } finally {
            HibernateUtil.closeSession(session);
        }
    }    
    

    @Override
    public List<Good> getGoods() {
        session = HibernateUtil.getSessionFactory().openSession();
        try {
            List<Good> list = session.createCriteria(Good.class
            ).list();
            return list;
        } catch (HibernateException he) {
            logger.error(he);
        } finally {
            HibernateUtil.closeSession(session);
        }
        return null;
    }

    @Override
    public void deleteGoodById(int id) {
        session = HibernateUtil.getSessionFactory().openSession();
        tx = session.beginTransaction();
        try {
            Query query = session.createQuery("from Good where id = :id");
            query.setInteger("id", id);
            Good good = (Good) query.uniqueResult();

            for (UserOrder uo : good.getOrders()) {
                session.delete(uo);
            }
            session.delete(good);
            tx.commit();
        } catch (Exception ex) {
            tx.rollback();
            logger.error(ex);
        } finally {
            HibernateUtil.closeSession(session);
        }
    }

}
