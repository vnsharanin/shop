package com.shar.shop.dao.hibernate;

import com.shar.shop.configs.hibernate.HibernateUtil;
import com.shar.shop.api.UserDAO;
import com.shar.shop.entities.hibernate.User;
import com.shar.shop.entities.hibernate.UserOrder;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class UserDAOImplHibernate implements UserDAO {

    private static final Logger logger = Logger.getLogger(UserDAOImplHibernate.class);

    public Session session = null;
    Transaction tx = null;

    @Override
    public void saveUser(User user) {
        
        session = HibernateUtil.getSessionFactory().openSession();
        //session.reconnect(null);
        //session = HibernateUtil.getSessionFactory().getCurrentSession();
        tx = session.beginTransaction();
        try {
            session.save(user);
            tx.commit();
        } catch (Exception ex) {
            tx.rollback();
            logger.error(ex);
        } finally {
            HibernateUtil.closeSession(session);
        }
    }

    @Override
    public void updateUser(User user) {
        session = HibernateUtil.getSessionFactory().openSession();
        //session = HibernateUtil.getSessionFactory().getCurrentSession();
        tx = session.beginTransaction();
        try {
            session.update(user);
            tx.commit();
        } catch (Exception ex) {
            tx.rollback();
            logger.error(ex);
        } finally {
            HibernateUtil.closeSession(session);
        }
    }

    @Override
    public void deleteUserById(int id) {
        session = HibernateUtil.getSessionFactory().openSession();
        tx = session.beginTransaction();
        try {
            Query query = session.createQuery("from User where id = :id");
            query.setInteger("id", id);
            User user = (User) query.uniqueResult();

            for (UserOrder uo : user.getOrders()) {
                session.delete(uo);
            }
            session.delete(user);
            tx.commit();
        } catch (Exception ex) {
            tx.rollback();
            logger.error(ex);
        } finally {
            HibernateUtil.closeSession(session);
        }
    }

    @Override
    public List<User> getUsers() {
        session = HibernateUtil.getSessionFactory().openSession();
        try {
            List<User> list = session.createCriteria(User.class).list();
            return list;
        } catch (HibernateException e) {
            logger.error(e);
        } finally {
            HibernateUtil.closeSession(session);
        }
        return null;
    }

    @Override
    public User getUserById(int id) {
        session = HibernateUtil.getSessionFactory().openSession();
        try {
            Query query = session.createQuery("from User where id = :id");
            query.setInteger("id", id);
            return (User) query.uniqueResult();
        } catch (Exception ex) {
            logger.error(ex);
        } finally {
            HibernateUtil.closeSession(session);
        }
        return null;
    }
}
