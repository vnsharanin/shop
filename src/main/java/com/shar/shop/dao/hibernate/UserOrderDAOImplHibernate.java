package com.shar.shop.dao.hibernate;

import com.shar.shop.api.UserOrderDAO;
import com.shar.shop.configs.hibernate.HibernateUtil;
import com.shar.shop.entities.hibernate.UserOrder;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class UserOrderDAOImplHibernate implements UserOrderDAO {

    private static final Logger logger = Logger.getLogger(UserOrderDAOImplHibernate.class);

    private Session session = null;
    private Transaction tx = null;

    @Override
    public void saveOrder(UserOrder order) {
        session = HibernateUtil.getSessionFactory().openSession();
        tx = session.beginTransaction();
        try {
            session.save(order);
            tx.commit();
        } catch (HibernateException he) {
            tx.rollback();
            logger.error(he);
        } finally {
            HibernateUtil.closeSession(session);
        }
    }

        @Override
    public void updateOrder(UserOrder order) {
        session = HibernateUtil.getSessionFactory().openSession();
        tx = session.beginTransaction();
        try {
            session.update(order);
            tx.commit();
        } catch (HibernateException he) {
            tx.rollback();
            logger.error(he);
        } finally {
            HibernateUtil.closeSession(session);
        }
    }
    
    @Override
    public UserOrder getOrderById(int id) {
        session = HibernateUtil.getSessionFactory().openSession();
        try {
            Query query = session.createQuery("from UserOrder where id = :id");
            query.setInteger("id", id);
            return (UserOrder) query.uniqueResult();
        } catch (Exception ex) {
            logger.error(ex);
        } finally {
            HibernateUtil.closeSession(session);
        }
        return null;
    }

    @Override
    public List<UserOrder> getAllOrders() {
        session = HibernateUtil.getSessionFactory().openSession();
        try {
            List<UserOrder> list = session.createCriteria(UserOrder.class).list();
            return list;
        } catch (HibernateException e) {
            logger.error(e);
        } finally {
            HibernateUtil.closeSession(session);
        }
        return null;
    }

    @Override
    public void deleteOrderById(int id) {
        session = HibernateUtil.getSessionFactory().openSession();
        tx = session.beginTransaction();
        try {
            Query query = session.createQuery("from UserOrder where id = :id");
            query.setInteger("id", id);
            session.delete((UserOrder) query.uniqueResult());
            tx.commit();
        } catch (HibernateException e) {
            tx.rollback();
            logger.error(e);
        } finally {
            HibernateUtil.closeSession(session);
        }
    }

}
