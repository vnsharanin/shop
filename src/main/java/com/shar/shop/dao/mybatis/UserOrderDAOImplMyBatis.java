package com.shar.shop.dao.mybatis;

import com.shar.shop.api.UserOrderDAO;
import com.shar.shop.configs.mybatis.MyBatisSqlSessionFactory;
import com.shar.shop.entities.hibernate.UserOrder;
import java.util.List;
import org.apache.ibatis.session.SqlSession;

import org.apache.log4j.Logger;

public class UserOrderDAOImplMyBatis implements UserOrderDAO {

    private static final Logger logger = Logger.getLogger(UserOrderDAOImplMyBatis.class);

    @Override
    public UserOrder getOrderById(int id) {
        SqlSession session = MyBatisSqlSessionFactory.getSqlSessionFactory().openSession();
        try {
            UserOrderDAO userOrderMapper = session.getMapper(UserOrderDAO.class);
            return userOrderMapper.getOrderById(id);
        } catch (Exception ex) {
            logger.error(ex);
        } finally {
            session.close();
        }
        return null;
    }

    @Override
    public void saveOrder(UserOrder order) {
        SqlSession session = MyBatisSqlSessionFactory.getSqlSessionFactory().openSession();
        try {
            UserOrderDAO userOrderMapper = session.getMapper(UserOrderDAO.class);
            userOrderMapper.saveOrder(order);
            session.commit();
        } catch (Exception ex) {
            session.rollback();
            logger.error(ex);
        } finally {
            session.close();
        }
    }

    @Override
    public void updateOrder(UserOrder order) {
        SqlSession session = MyBatisSqlSessionFactory.getSqlSessionFactory().openSession();
        try {
            UserOrderDAO userOrderMapper = session.getMapper(UserOrderDAO.class);
            userOrderMapper.updateOrder(order);
            session.commit();
        } catch (Exception ex) {
            session.rollback();
            logger.error(ex);
        } finally {
            session.close();
        }
    }

    @Override
    public List<UserOrder> getAllOrders() {
        SqlSession session = MyBatisSqlSessionFactory.getSqlSessionFactory().openSession();
        try {
            UserOrderDAO userOrderMapper = session.getMapper(UserOrderDAO.class);
            return userOrderMapper.getAllOrders();
        } catch (Exception ex) {
            logger.error(ex);
        } finally {
            session.close();
        }
        return null;
    }

    @Override
    public void deleteOrderById(int id) {
        SqlSession session = MyBatisSqlSessionFactory.getSqlSessionFactory().openSession();
        try {
            UserOrderDAO userOrderMapper = session.getMapper(UserOrderDAO.class);
            userOrderMapper.deleteOrderById(id);
            session.commit();
        } catch (Exception ex) {
            session.rollback();
            logger.error(ex);
        } finally {
            session.close();
        }
    }
}
