package com.shar.shop.dao.mybatis;

import com.shar.shop.api.UserDAO;
import com.shar.shop.configs.mybatis.MyBatisSqlSessionFactory;
import com.shar.shop.entities.hibernate.User;
import java.util.List;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

public class UserDAOImplMyBatis implements UserDAO {

    private static final Logger logger = Logger.getLogger(UserDAOImplMyBatis.class);

    @Override
    public void saveUser(User user) {
        SqlSession session = MyBatisSqlSessionFactory.getSqlSessionFactory().openSession();
        try {
            UserDAO userMapper = session.getMapper(UserDAO.class);
            userMapper.saveUser(user);
            session.commit();
        } catch (Exception ex) {
            session.rollback();
            logger.error(ex);
        } finally {
            session.close();
        }
    }

    @Override
    public void updateUser(User user) {
        SqlSession session = MyBatisSqlSessionFactory.getSqlSessionFactory().openSession();
        try {
            UserDAO userMapper = session.getMapper(UserDAO.class);
            userMapper.updateUser(user);
            session.commit();
        } finally {
            session.close();
        }
    }

    @Override
    public List<User> getUsers() {
        SqlSession session = MyBatisSqlSessionFactory.getSqlSessionFactory().openSession();
        try {
            UserDAO userMapper = session.getMapper(UserDAO.class);
            return userMapper.getUsers();
        } catch (Exception ex) {
            logger.error(ex);
        } finally {
            session.close();
        }
        return null;
    }

    @Override
    public void deleteUserById(int id) {
        SqlSession session = MyBatisSqlSessionFactory.getSqlSessionFactory().openSession();
        try {
            UserDAO userMapper = session.getMapper(UserDAO.class);
            userMapper.deleteUserById(id);
            session.commit();
        } catch (Exception ex) {
            session.rollback();
            logger.error(ex);
        } finally {
            session.close();
        }
    }

    @Override
    public User getUserById(int id) {
        SqlSession session = MyBatisSqlSessionFactory.getSqlSessionFactory().openSession();
        try {
            UserDAO userMapper = session.getMapper(UserDAO.class);
            return userMapper.getUserById(id);
        } catch (Exception ex) {
            logger.error(ex);
        } finally {
            session.close();
        }
        return null;
    }
}
