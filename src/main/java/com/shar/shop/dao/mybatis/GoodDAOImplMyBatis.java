package com.shar.shop.dao.mybatis;

import com.shar.shop.api.GoodDAO;
import com.shar.shop.configs.mybatis.MyBatisSqlSessionFactory;
import com.shar.shop.entities.hibernate.Good;
import java.util.List;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

public class GoodDAOImplMyBatis implements GoodDAO {

    private static final Logger logger = Logger.getLogger(GoodDAOImplMyBatis.class);

    @Override
    public Good getGoodById(int id) {
        SqlSession session = MyBatisSqlSessionFactory.getSqlSessionFactory().openSession();
        try {
            GoodDAO goodMapper = session.getMapper(GoodDAO.class);
            return goodMapper.getGoodById(id);
        } catch (Exception ex) {
            logger.error(ex);
        } finally {
            session.close();
        }
        return null;
    }

    @Override
    public void saveGood(Good good) {
        SqlSession session = MyBatisSqlSessionFactory.getSqlSessionFactory().openSession();
        try {
            GoodDAO goodMapper = session.getMapper(GoodDAO.class);
            goodMapper.saveGood(good);
            session.commit();
        } catch (Exception ex) {
            session.rollback();
            logger.error(ex);
        } finally {
            session.close();
        }
    }

    @Override
    public void updateGood(Good good) {
        SqlSession session = MyBatisSqlSessionFactory.getSqlSessionFactory().openSession();
        try {
            GoodDAO goodMapper = session.getMapper(GoodDAO.class);
            goodMapper.updateGood(good);
            session.commit();
        } catch (Exception ex) {
            session.rollback();
            logger.error(ex);
        } finally {
            session.close();
        }
    }

    @Override
    public List<Good> getGoods() {
        SqlSession session = MyBatisSqlSessionFactory.getSqlSessionFactory().openSession();
        try {
            GoodDAO goodMapper = session.getMapper(GoodDAO.class);
            return goodMapper.getGoods();
        } catch (Exception ex) {
            logger.error(ex);
        } finally {
            session.close();
        }
        return null;
    }

    @Override
    public void deleteGoodById(int id) {
        SqlSession session = MyBatisSqlSessionFactory.getSqlSessionFactory().openSession();
        try {
            GoodDAO goodMapper = session.getMapper(GoodDAO.class);
            goodMapper.deleteGoodById(id);
            session.commit();
        } catch (Exception ex) {
            session.rollback();
            logger.error(ex);
        } finally {
            session.close();
        }
    }
}
