package com.shar.shop;

import com.shar.shop.entities.hibernate.User;
import com.shar.shop.entities.hibernate.UserOrder;
import com.shar.shop.api.UserDAO;
import com.shar.shop.api.UserOrderDAO;
import com.shar.shop.dao.mybatis.UserDAOImplMyBatis;
import com.shar.shop.dao.hibernate.UserDAOImplHibernate;
import com.shar.shop.dao.hibernate.UserOrderDAOImplHibernate;
import com.shar.shop.dao.mybatis.UserOrderDAOImplMyBatis;
import com.shar.shop.entities.speedment.shop.ShopApplication;
import com.shar.shop.entities.speedment.shop.shop.shop.user_order.impl.UserOrderImpl;
import com.shar.shop.entities.speedment.shop.shop.shop.users.Users;
import com.shar.shop.entities.speedment.shop.shop.shop.users.impl.UsersImpl;
import com.speedment.Manager;
import com.speedment.Speedment;
import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Stream;
import org.apache.log4j.Logger;
import org.perf4j.StopWatch;
import org.perf4j.log4j.Log4JStopWatch;

import com.shar.shop.test.mybatis.*;
import com.shar.shop.test.hibernate.*;
import com.shar.shop.test.speedment.*;
import java.util.logging.Level;

public class Main {

    private static final Logger logger = Logger.getLogger(Main.class);

    public Main() {
    }

    public static void main(String[] args) {
       myBatis();
    //   speedment();
      // hibernate();
    }

    private static void hibernate() {
        UserDAOImplHibernateTest userTestHibernate = new UserDAOImplHibernateTest();
        GoodDAOImplHibernateTest goodTestHibernate = new GoodDAOImplHibernateTest();
        UserOrderDAOImplHibernateTest orderTestHibernate = new UserOrderDAOImplHibernateTest();
        
userTestHibernate.saveUserTest100000Records();
goodTestHibernate.saveGoodTest100000Records();
orderTestHibernate.saveOrderTest100000Records();
        
        
        
        
     //   userTestHibernate.getUsers30Threads();
      /*  userTestHibernate.getUsers10Threads();
        userTestHibernate.getUsers20Threads();
        goodTestHibernate.getGoods10Threads();
        goodTestHibernate.getGoods20Threads();
        orderTestHibernate.getAllOrders10Threads();
        orderTestHibernate.getAllOrders20Threads();*/
    }

    private static void speedment() {
        UserImplTest userTestSpeedment = new UserImplTest();
        GoodImplTest goodTestSpeedment = new GoodImplTest();
        UserOrderImplTest orderTestSpeedment = new UserOrderImplTest();
       userTestSpeedment.saveUserTest1000Threads1000000Records();
         goodTestSpeedment.saveGoodTest1000Threads1000000Records();
      
      /*  userTestSpeedment.saveUserTest100000Records();
        goodTestSpeedment.saveGoodTest100000Records();
        orderTestSpeedment.saveOrderTest100000Records();*/
           
      /*   userTestSpeedment.getUsers10Threads();
       userTestSpeedment.getUsers20Threads();
        goodTestSpeedment.getGoods10Threads();
         goodTestSpeedment.getGoods20Threads();
        orderTestSpeedment.getOrders10Threads();
         orderTestSpeedment.getOrders20Threads();*/
    }

    private static void myBatis() {
        UserDAOImplMyBatisTest userTestMyBatis = new UserDAOImplMyBatisTest();
        GoodDAOImplMyBatisTest goodTestMyBatis = new GoodDAOImplMyBatisTest();
        UserOrderDAOImplMyBatisTest orderTestMyBatis = new UserOrderDAOImplMyBatisTest();
userTestMyBatis.saveUserTest1000Threads1000000Records();
goodTestMyBatis.saveGoodTest1000Threads1000000Records();

     /*   userTestMyBatis.saveUserTest100000Records();
        goodTestMyBatis.saveGoodTest100000Records();
        orderTestMyBatis.saveOrderTest100000Records();*/
                
     /*   userTestMyBatis.getUsers10Threads();
        userTestMyBatis.getUsers20Threads();
        goodTestMyBatis.getGoods10Threads();
        goodTestMyBatis.getGoods20Threads();
      orderTestMyBatis.getAllOrders10Threads();
         orderTestMyBatis.getAllOrders20Threads();*/
    }

}
